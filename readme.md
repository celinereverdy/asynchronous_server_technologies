# Asynchronous Server Technologies

## This project is for :
* Creating a server with NodeJS

## Run :
* `npm start`

## Test :
* `npm test`

## Contributors :
* Céline Reverdy
